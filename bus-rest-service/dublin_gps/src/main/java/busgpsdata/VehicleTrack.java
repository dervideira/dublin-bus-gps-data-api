package busgpsdata;

public class VehicleTrack implements Track {

    private double lat;
    private double lon;
    private long unixTimespan;

    public VehicleTrack(double lat, double lon, long unixTimespan) {
        this.lat = lat;
        this.lon = lon;
        this.unixTimespan = unixTimespan;
    }

    @Override
    public double getLat() {
        return lat;
    }

    @Override
    public double getLon() {
        return lon;
    }

    @Override
    public long getUnixTimespan() {
        return unixTimespan;
    }
}
