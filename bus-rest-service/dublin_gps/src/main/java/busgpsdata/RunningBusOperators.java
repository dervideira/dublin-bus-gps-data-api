package busgpsdata;

import java.util.ArrayList;
import java.util.List;

public class RunningBusOperators {

    private List<String> runningOperatorIds;

    public RunningBusOperators() {
        this.runningOperatorIds = new ArrayList<>();
    }

    public void add(Operator operator) {
        if (!runningOperatorIds.contains(operator.getOperatorId())) {
            runningOperatorIds.add(operator.getOperatorId());
        }
    }

    public List<Operator> getRunningOperators() {
        List<Operator> outList = new ArrayList<>();
        for (String runningOperatorId : runningOperatorIds) {
            outList.add(new DublinBusOperator(runningOperatorId));
        }
        return outList;
    }
}
