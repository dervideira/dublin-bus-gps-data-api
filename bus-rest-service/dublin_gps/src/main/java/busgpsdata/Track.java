package busgpsdata;

public interface Track {

    double getLat();

    double getLon();

    long getUnixTimespan();

}
