package busgpsdata;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class DublinBusVehicle implements Vehicle {

    private String operatorId;
    private int vehicleId;
    private boolean atStop;
    private List<Track> tracks;

    public DublinBusVehicle(int vehicleId) {
        setVehicleId(vehicleId);
        tracks = new ArrayList<>();
    }

    public DublinBusVehicle(int vehicleId, Operator operator, boolean atStop) {
        setVehicleId(vehicleId);
        setOperator(operator);
        setAtStop(atStop);
        tracks = new ArrayList<>();
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public void setOperator(Operator operator) {
        this.operatorId = operator.getOperatorId();
    }

    public void setAtStop(boolean atStop) {
        this.atStop = atStop;
    }

    @Override
    public List<Track> getTracks() {
        return this.tracks;
    }

    @Override
    public String getVehicleOperatorId() {
        return operatorId;
    }

    @Override
    public int getVehicleId() {
        return vehicleId;
    }

    @Override
    public boolean isAtStop() {
        return atStop;
    }

    @Override
    public void setTrack(long unixTimestampStart, long unixTimestampEnd) {
        try {
            DatabaseConn db = new DatabaseConn();
            FindIterable<Document> docs = db.collection
                    .find(Filters.and(
                            Filters.gte(DatabaseConn.TIMESTAMP_FIELD_NAME, unixTimestampStart),
                            Filters.lt(DatabaseConn.TIMESTAMP_FIELD_NAME, unixTimestampEnd),
                            Filters.eq(DatabaseConn.VEHICLE_FIELD_NAME, this.getVehicleId())
                    )).sort(Sorts.ascending(DatabaseConn.TIMESTAMP_FIELD_NAME));
            setOperator(new DublinBusOperator(docs.first().getString(DatabaseConn.OPERATOR_FIELD_NAME)));
            for (Document doc : docs) {
                double lat = doc.getDouble(DatabaseConn.LAT_FIELD_NAME);
                double lon = doc.getDouble(DatabaseConn.LON_FIELD_NAME);
                long unixTimespan = doc.getLong(DatabaseConn.TIMESTAMP_FIELD_NAME);
                this.tracks.add(new VehicleTrack(lat, lon, unixTimespan));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
