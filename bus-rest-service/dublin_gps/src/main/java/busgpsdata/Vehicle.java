package busgpsdata;

import java.util.List;

public interface Vehicle {

    String getVehicleOperatorId();

    void setOperator(Operator operator);

    int getVehicleId();

    void setVehicleId(int vehicleId);

    boolean isAtStop();

    void setAtStop(boolean atStop);

    void setTrack(long unixTimestampStart, long unixTimestampEnd);

    List<Track> getTracks();
}
