package busgpsdata;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class DatabaseConn {

    private static String databaseName = "dublin_bus_dataset";
    private static String collName = "bus_gps_data";
    public static String OPERATOR_FIELD_NAME = "Operator";
    public static String TIMESTAMP_FIELD_NAME = "UnixTimestamp";
    public static String VEHICLE_FIELD_NAME = "VehicleID";
    public static String AT_STOP_FIELD_NAME = "AtStop";
    public static String LAT_FIELD_NAME = "Lat";
    public static String LON_FIELD_NAME = "Lon";

    protected MongoClient mongoClient = MongoClients.create();
    protected MongoDatabase database = mongoClient.getDatabase(databaseName);
    protected MongoCollection collection = database.getCollection(collName);

    public DatabaseConn() {
        // Nothing here
    }

}
