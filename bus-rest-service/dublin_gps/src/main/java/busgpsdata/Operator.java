package busgpsdata;

import java.util.List;

public interface Operator {

    String getOperatorId();
    void setOperatorVehicles(long unixTimestampStart, long unixTimestampEnd);
    void setOperatorVehiclesAtStop(long unixTimestampStart, long unixTimestampEnd);
    List<Vehicle> getOperatorVehicles();

}
