package busgpsdata;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

public class DublinBusOperator implements Operator {

    private String operatorId;
    private List<Vehicle> operatorVehicles;

    public DublinBusOperator(String operatorId) {
        setOperatorId(operatorId);
        operatorVehicles = new ArrayList<>();
    }

    private void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    @Override
    public String getOperatorId() {
        return operatorId;
    }

    private void internalSetOperatorVehicles(long unixTimestampStart, long unixTimestampEnd, boolean onlyAtAtop) {
        Bson filter = null;
        if(onlyAtAtop == true) {
            filter = Filters.and(
                    Filters.gte(DatabaseConn.TIMESTAMP_FIELD_NAME, unixTimestampStart),
                    Filters.lt(DatabaseConn.TIMESTAMP_FIELD_NAME, unixTimestampEnd),
                    Filters.eq(DatabaseConn.OPERATOR_FIELD_NAME, this.getOperatorId()),
                    Filters.eq(DatabaseConn.AT_STOP_FIELD_NAME, true)
            );
        } else {
            filter = Filters.and(
                    Filters.gte(DatabaseConn.TIMESTAMP_FIELD_NAME, unixTimestampStart),
                    Filters.lt(DatabaseConn.TIMESTAMP_FIELD_NAME, unixTimestampEnd),
                    Filters.eq(DatabaseConn.OPERATOR_FIELD_NAME, this.getOperatorId())
            );
        }

        try {
            DatabaseConn db = new DatabaseConn();
            FindIterable<Document> docs = db.collection
                    .find(filter)
                    .sort(Sorts.ascending(DatabaseConn.OPERATOR_FIELD_NAME));
            for (Document doc : docs) {
                this.operatorVehicles.add(createVehicle(doc));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public void setOperatorVehicles(long unixTimestampStart, long unixTimestampEnd) {
        this.internalSetOperatorVehicles(unixTimestampStart, unixTimestampEnd, false);
    }

    @Override
    public void setOperatorVehiclesAtStop(long unixTimestampStart, long unixTimestampEnd) {
        this.internalSetOperatorVehicles(unixTimestampStart, unixTimestampEnd, true);
    }

    @Override
    public List<Vehicle> getOperatorVehicles() {
        return this.operatorVehicles;
    }

    private static Vehicle createVehicle(Document doc) {
        int vehicleId = doc.getInteger(DatabaseConn.VEHICLE_FIELD_NAME);
        Operator vehicleOperator = new DublinBusOperator(doc.getString(DatabaseConn.OPERATOR_FIELD_NAME));
        boolean atStop = doc.getBoolean(DatabaseConn.AT_STOP_FIELD_NAME);
        return new DublinBusVehicle(vehicleId, vehicleOperator, atStop);
    }

    public static List<Operator> getRunningOperators(long unixTimestampStart, long unixTimestampEnd) {
        List<Operator> outList = new ArrayList<>();
        RunningBusOperators runningBusOperators = new RunningBusOperators();
        try {
            DatabaseConn db = new DatabaseConn();
            FindIterable<Document> docs = db.collection
                    .find(Filters.and(
                            Filters.gte(DatabaseConn.TIMESTAMP_FIELD_NAME, unixTimestampStart),
                            Filters.lt(DatabaseConn.TIMESTAMP_FIELD_NAME, unixTimestampEnd)
                    ))
                    .sort(Sorts.ascending(DatabaseConn.OPERATOR_FIELD_NAME));
            //.distinct(DatabaseConn.OPERATOR_FIELD_NAME, String.class).iterator();
            for (Document doc : docs) {
                runningBusOperators.add(new DublinBusOperator(doc.getString(DatabaseConn.OPERATOR_FIELD_NAME)));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return runningBusOperators.getRunningOperators();
    }
}
