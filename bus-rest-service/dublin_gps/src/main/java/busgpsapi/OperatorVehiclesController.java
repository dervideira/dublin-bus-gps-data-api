package busgpsapi;

import busgpsdata.DublinBusOperator;
import busgpsdata.DublinBusVehicle;
import busgpsdata.Operator;
import busgpsdata.Vehicle;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OperatorVehiclesController {

    @RequestMapping(value = "/operatorVehicles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<Vehicle> operatorVehicles(@RequestParam(value = "startunixtime", defaultValue = "1352160002000000") Long startTime,
                                   @RequestParam(value = "endunixtime", defaultValue = "1352160004000000") Long endTime,
                                   @RequestParam(value = "operatorId", defaultValue = "SL") String operatorId) {
		Operator operator = new DublinBusOperator(operatorId);
        operator.setOperatorVehicles(startTime, endTime);
        return operator.getOperatorVehicles();
    }

	@RequestMapping(value = "/vehiclesAtStop", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	List<Vehicle> vehiclesAtStop(@RequestParam(value = "startunixtime", defaultValue = "1352160002000000") Long startTime,
								   @RequestParam(value = "endunixtime", defaultValue = "1352160004000000") Long endTime,
								   @RequestParam(value = "operatorId", defaultValue = "SL") String operatorId) {
		Operator operator = new DublinBusOperator(operatorId);
		operator.setOperatorVehiclesAtStop(startTime, endTime);
		return operator.getOperatorVehicles();
	}

	@RequestMapping(value = "/vehicleTrack", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Vehicle vehicleTrack(@RequestParam(value = "startunixtime", defaultValue = "1352160002000000") Long startTime,
								 @RequestParam(value = "endunixtime", defaultValue = "1352160004000000") Long endTime,
								 @RequestParam(value = "vehicleId", defaultValue = "33631") int vehicleId) {
		Vehicle vehicle = new DublinBusVehicle(vehicleId);
		vehicle.setTrack(startTime, endTime);
		return vehicle;
	}

}
