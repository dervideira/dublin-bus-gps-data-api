package busgpsapi;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import busgpsdata.DublinBusOperator;
import busgpsdata.Operator;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class RunningOperatorsController {

    @RequestMapping(value = "/runningOperators", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<Operator> runningOperators(@RequestParam(value = "startunixtime", defaultValue = "1352160002000000") Long startTime,
                                    @RequestParam(value = "endunixtime", defaultValue = "1352160004000000") Long endTime) {
        List<Operator> operators = DublinBusOperator.getRunningOperators(startTime, endTime);
        return operators;
    }

}
