/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	  https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package busgpsapi;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BusGpsApiTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void runningOperatorsTest() throws Exception {

        String response = this.mockMvc.perform(get("/runningOperators")
                .param("startunixtime", "1352160002000000")
                .param("endunixtime", "1352160004000000")
        ).andDo(print()).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        Assert.assertEquals(response, "[{\"operatorId\":\"CD\",\"operatorVehicles\":[]},{\"operatorId\":\"CF\",\"operatorVehicles\":[]},{\"operatorId\":\"D1\",\"operatorVehicles\":[]},{\"operatorId\":\"D2\",\"operatorVehicles\":[]},{\"operatorId\":\"HN\",\"operatorVehicles\":[]},{\"operatorId\":\"PO\",\"operatorVehicles\":[]},{\"operatorId\":\"RD\",\"operatorVehicles\":[]},{\"operatorId\":\"SL\",\"operatorVehicles\":[]}]");
    }

    @Test
    public void operatorVehiclesTest() throws Exception {

        String response = this.mockMvc.perform(get("/operatorVehicles")
                .param("startunixtime", "1352160000000000")
                .param("endunixtime", "1352160008000000")
                .param("operatorId", "SL")
        ).andDo(print()).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        Assert.assertEquals(response, "[{\"vehicleId\":33631,\"atStop\":true,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":33633,\"atStop\":true,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":40039,\"atStop\":true,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":33626,\"atStop\":false,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":38039,\"atStop\":true,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":33273,\"atStop\":true,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":33127,\"atStop\":false,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":40046,\"atStop\":false,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":33111,\"atStop\":false,\"tracks\":[],\"vehicleOperatorId\":\"SL\"}]");
    }

    @Test
    public void vehiclesAtStopTest() throws Exception {

        String response = this.mockMvc.perform(get("/vehiclesAtStop")
                .param("startunixtime", "1352160000000000")
                .param("endunixtime", "1352160004000000")
                .param("operatorId", "SL")
        ).andDo(print()).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        Assert.assertEquals(response, "[{\"vehicleId\":33631,\"atStop\":true,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":33633,\"atStop\":true,\"tracks\":[],\"vehicleOperatorId\":\"SL\"},{\"vehicleId\":40039,\"atStop\":true,\"tracks\":[],\"vehicleOperatorId\":\"SL\"}]");
    }

    @Test
    public void vehicleTrackTest() throws Exception {

        String response = this.mockMvc.perform(get("/vehicleTrack")
                .param("startunixtime", "1352160000000000")
                .param("endunixtime", "1352160400000000")
                .param("vehicleId", "33631")
        ).andDo(print()).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        Assert.assertEquals(response, "{\"vehicleId\":33631,\"atStop\":false,\"tracks\":[{\"lat\":53.453217,\"lon\":-6.264167,\"unixTimespan\":1352160002000000},{\"lat\":53.451035,\"lon\":-6.261317,\"unixTimespan\":1352160023000000},{\"lat\":53.448318,\"lon\":-6.26135,\"unixTimespan\":1352160041000000},{\"lat\":53.445648,\"lon\":-6.263017,\"unixTimespan\":1352160061000000},{\"lat\":53.443317,\"lon\":-6.262866,\"unixTimespan\":1352160083000000},{\"lat\":53.440716,\"lon\":-6.263933,\"unixTimespan\":1352160102000000},{\"lat\":53.440048,\"lon\":-6.260933,\"unixTimespan\":1352160122000000},{\"lat\":53.439434,\"lon\":-6.2557,\"unixTimespan\":1352160142000000},{\"lat\":53.43705,\"lon\":-6.252,\"unixTimespan\":1352160164000000},{\"lat\":53.436481,\"lon\":-6.246867,\"unixTimespan\":1352160183000000},{\"lat\":53.437382,\"lon\":-6.2417,\"unixTimespan\":1352160203000000},{\"lat\":53.434216,\"lon\":-6.22885,\"unixTimespan\":1352160262000000},{\"lat\":53.43285,\"lon\":-6.229483,\"unixTimespan\":1352160284000000},{\"lat\":53.430134,\"lon\":-6.22985,\"unixTimespan\":1352160302000000},{\"lat\":53.428185,\"lon\":-6.226567,\"unixTimespan\":1352160323000000},{\"lat\":53.427868,\"lon\":-6.221217,\"unixTimespan\":1352160343000000},{\"lat\":53.427799,\"lon\":-6.216833,\"unixTimespan\":1352160367000000},{\"lat\":53.425968,\"lon\":-6.217417,\"unixTimespan\":1352160381000000}],\"vehicleOperatorId\":\"SL\"}");
    }

}
