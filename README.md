
# Dublin bus GPS data api

This is a RESTful API for Daimler challenge using the Dublin Buses GPS data set

## What can this do?

There are four endpoints available to gather data:

1. Given a time frame [start-time, end-time], what is the list of running operators?

2. Given a time frame [start-time, end-time] and an Operator, what is the list of vehicle IDs?

3. Given a time frame [start-time, end-time] and a fleet, which vehicles are at a stop?

4. Given a time frame [start-time, end-time] and a vehicle, return the trace of that vehicle (GPS entries, ordered by timestamp).

Time frames must be passed in **unix timestamp** format (micro seconds since 1970 01 01 00:00:00 GMT).

## Technologies used

* Java with OpenJDK 1.8
    - A well known and widely used type-safe programming language which is cross platform capable
* Spring boot
    - A mature web framework with good performance and with the capability of running stand alone, which is useful for this kind of project
* Python 3
    - Also a well known and widely used programming language which was, in this case, used to create a script to populate data into the database
* MongoDB
    - A NoSQL document based database capable of dealing with large amounts of data, which encourages trading disk space for processing time. Each GPS data entry can be considered a document which describes each timespan data point. This database also leaves the door open for creating other useful collections such a collection of operators with a list of its vehicles which could be useful for faster queries that do not depend on timespans

## How to run this

* Clone this repository

* Install MongoDB using the [official instructions for your operating system](https://docs.mongodb.com/manual/installation/)
    - Start MongoDB listening on `localhost` on default port `27017`
* Download the sample data fom [here](https://codechallengestracc.blob.core.windows.net/code-challenge/dublin-dataset.zip)
    - The csv files should be inside this repository in folder `dublin-dataset/dublin-dataset`
* Install Python3 using the [official instructions for your operating system](https://www.python.org/downloads/)
        - If you are with Ubuntu `apt install python3` will do the job
    - Install pymongo: `python3 -m pip install pymongo`
        - Or use requirements.txt file: `python3 -m pip install -r requirements.txt`
    - Populate the database `python3 populate_data.py`
        - Default csv data file is `dublin-dataset/dublin-dataset/siri.20121106.csv`
        - This file can be changed inside `populate_data.py` by changing the `CSV_PATH` variable value
    - As an alternative, you can download the database json dump and restore it to a local MongoDB (instructions valid for Unix based systems)
        - `wget https://bitbucket.org/dervideira/dublin-bus-gps-data-api/downloads/dublin_bus_dataset.zip`
        - `unzip dublin_bus_dataset.zip`
        - `mongoimport -d dublin_bus_dataset -c bus_gps_data  --file out.json`
* Create MongoDB index on timestamp on the mongo shell
    - `db.getCollection('bus_gps_data').createIndex({"UnixTimestamp" : 1})`
        - This is not mandatory but helps the queries run faster
* `cd` into `bus-rest-service/dublin_gps`
* Run `./gradlew build` to build the project
    - Java must exist in the system
* Run `./gradlew bootRun` to start the application
    - `./gradlew` command is suitable for unix system. For Windows, use `gradlew.bat`

## How to use it

You should requests one of the four endpoints available: `/runningOperators`, `/operatorVehicles`, `/vehiclesAtStop` and `/vehicleTrack`.

Each endpoint can receive parameters. Please check the examples to see what parameters are valid for each one.

* Using a browser
    - Url for endpoint #1:
        - [http://localhost:8080/runningOperators?startunixtime=1352160002000000&endunixtime=1352160004000000](http://localhost:8080/runningOperators?startunixtime=1352160002000000&endunixtime=1352160004000000)
    - Expected reply for endpoint #1:
        - `[{"operatorId":"CD","operatorVehicles":[]},{"operatorId":"CF","operatorVehicles":[]},{"operatorId":"D1","operatorVehicles":[]},{"operatorId":"D2","operatorVehicles":[]},{"operatorId":"HN","operatorVehicles":[]},{"operatorId":"PO","operatorVehicles":[]},{"operatorId":"RD","operatorVehicles":[]},{"operatorId":"SL","operatorVehicles":[]}]`
    - Url for endpoint #2:
        - [http://localhost:8080/operatorVehicles?startunixtime=1352160000000000&endunixtime=1352160008000000&operatorId=SL](http://localhost:8080/operatorVehicles?startunixtime=1352160000000000&endunixtime=1352160008000000&operatorId=SL)
    - Expected reply for endpoint #2:
        - `[{"vehicleId":33631,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":33633,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":40039,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":33626,"atStop":false,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":38039,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":33127,"atStop":false,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":40046,"atStop":false,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":33273,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":33111,"atStop":false,"tracks":[],"vehicleOperatorId":"SL"}]`
    - Url for endpoint #3:
        - [http://localhost:8080/vehiclesAtStop?startunixtime=1352160000000000&endunixtime=1352160004000000&operatorId=SL](http://localhost:8080/vehiclesAtStop?startunixtime=1352160000000000&endunixtime=1352160004000000&operatorId=SL)
    - Expected reply for endpoint #3:
        - `[{"vehicleId":33631,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":33633,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":40039,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"}]`
    - Url for endpoint #4:
        - [http://localhost:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160400000000&vehicleId=33631](http://localhost:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160400000000&vehicleId=33631)
    - Expected reply for endpoint #4:
        - `{"vehicleId":33631,"atStop":false,"tracks":[{"lat":53.453217,"lon":-6.264167,"unixTimespan":1352160002000000},{"lat":53.451035,"lon":-6.261317,"unixTimespan":1352160023000000},{"lat":53.448318,"lon":-6.26135,"unixTimespan":1352160041000000},{"lat":53.445648,"lon":-6.263017,"unixTimespan":1352160061000000},{"lat":53.443317,"lon":-6.262866,"unixTimespan":1352160083000000},{"lat":53.440716,"lon":-6.263933,"unixTimespan":1352160102000000},{"lat":53.440048,"lon":-6.260933,"unixTimespan":1352160122000000},{"lat":53.439434,"lon":-6.2557,"unixTimespan":1352160142000000},{"lat":53.43705,"lon":-6.252,"unixTimespan":1352160164000000},{"lat":53.436481,"lon":-6.246867,"unixTimespan":1352160183000000},{"lat":53.437382,"lon":-6.2417,"unixTimespan":1352160203000000},{"lat":53.434216,"lon":-6.22885,"unixTimespan":1352160262000000},{"lat":53.43285,"lon":-6.229483,"unixTimespan":1352160284000000},{"lat":53.430134,"lon":-6.22985,"unixTimespan":1352160302000000},{"lat":53.428185,"lon":-6.226567,"unixTimespan":1352160323000000},{"lat":53.427868,"lon":-6.221217,"unixTimespan":1352160343000000},{"lat":53.427799,"lon":-6.216833,"unixTimespan":1352160367000000},{"lat":53.425968,"lon":-6.217417,"unixTimespan":1352160381000000}],"vehicleOperatorId":"SL"}`

* These urls can also be tested with a tool like `curl -v <url>` for more details
```
david@laptop:~$ curl -v http://localhost:8080/vehiclesAtStop?startunixtime=1352160000000000&endunixtime=1352160004000000&operatorId=SL
[1] 28771
[2] 28772
david@laptop:~$ *   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /vehiclesAtStop?startunixtime=1352160000000000 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.58.0
> Accept: */*
>
< HTTP/1.1 200
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Wed, 15 Jan 2020 10:59:29 GMT
<
* Connection #0 to host localhost left intact
[{"vehicleId":33631,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":33633,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"},{"vehicleId":40039,"atStop":true,"tracks":[],"vehicleOperatorId":"SL"}]
```
    - 
* Or using Python3 (`requests` must be installed for this to work: `python3 -m pip install requests`)
```
>>> import requests
>>> r = requests.get('http://localhost:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160400000000&vehicleId=33631')
>>> r.json()
{'vehicleId': 33631, 'atStop': False, 'tracks': [{'lat': 53.453217, 'lon': -6.264167, 'unixTimespan': 1352160002000000}, {'lat': 53.451035, 'lon': -6.261317, 'unixTimespan': 1352160023000000}, {'lat': 53.448318, 'lon': -6.26135, 'unixTimespan': 1352160041000000}, {'lat': 53.445648, 'lon': -6.263017, 'unixTimespan': 1352160061000000}, {'lat': 53.443317, 'lon': -6.262866, 'unixTimespan': 1352160083000000}, {'lat': 53.440716, 'lon': -6.263933, 'unixTimespan': 1352160102000000}, {'lat': 53.440048, 'lon': -6.260933, 'unixTimespan': 1352160122000000}, {'lat': 53.439434, 'lon': -6.2557, 'unixTimespan': 1352160142000000}, {'lat': 53.43705, 'lon': -6.252, 'unixTimespan': 1352160164000000}, {'lat': 53.436481, 'lon': -6.246867, 'unixTimespan': 1352160183000000}, {'lat': 53.437382, 'lon': -6.2417, 'unixTimespan': 1352160203000000}, {'lat': 53.434216, 'lon': -6.22885, 'unixTimespan': 1352160262000000}, {'lat': 53.43285, 'lon': -6.229483, 'unixTimespan': 1352160284000000}, {'lat': 53.430134, 'lon': -6.22985, 'unixTimespan': 1352160302000000}, {'lat': 53.428185, 'lon': -6.226567, 'unixTimespan': 1352160323000000}, {'lat': 53.427868, 'lon': -6.221217, 'unixTimespan': 1352160343000000}, {'lat': 53.427799, 'lon': -6.216833, 'unixTimespan': 1352160367000000}, {'lat': 53.425968, 'lon': -6.217417, 'unixTimespan': 1352160381000000}], 'vehicleOperatorId': 'SL'}`
>>>
```

## What about scalability and latency?

* Response times could be improved by further improving MongoDB indexes 
* If API requests often return duplicated data, caching (Redis is a good option) could be used to improve latency 
* If a very large number of requests are observed, the application could be containerized (Docker is a good option) to enable easier replication in a Kubertenes cluster

## Live demo

There is a live demo of this api available. This demo is running on a small [DigitalOcean](https://www.digitalocean.com/) VPS and uses an [https://mlab.com](https://mlab.com) free tier database and it may be slower to responde. Also, because the free tier limit is 500 MB, the data is not exactly the same so replies might be different. 

* This database can be accessed with mongo shell like this `mongo ds263368.mlab.com:63368/dublin_bus_dataset -u dbus -p dbusdataset1`

### Live demo example requests

* [http://dme.ninja:8080/runningOperators?startunixtime=1352160002000000&endunixtime=1352160004000000](http://dme.ninja:8080/runningOperators?startunixtime=1352160002000000&endunixtime=1352160004000000)

* [http://dme.ninja:8080/operatorVehicles?startunixtime=1352160000000000&endunixtime=1352160008000000&operatorId=SL](http://dme.ninja:8080/operatorVehicles?startunixtime=1352160000000000&endunixtime=1352160008000000&operatorId=SL)

* [http://dme.ninja:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160190000000&vehicleId=33631](http://dme.ninja:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160190000000&vehicleId=33631)

* [http://dme.ninja:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160108000000&vehicleId=33633](http://dme.ninja:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160108000000&vehicleId=33633)

* [http://dme.ninja:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160104000000&vehicleId=33633](http://dme.ninja:8080/vehicleTrack?startunixtime=1352160000000000&endunixtime=1352160104000000&vehicleId=33633)
  



