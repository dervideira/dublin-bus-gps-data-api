import csv
import pymongo
from datetime import datetime

"""
Bus GPS Data Dublin Bus GPS data across Dublin City, from Dublin City Council'traffic control, in csv format. Each datapoint (row in the CSV file) has the following entries:
'
  0'Timestamp micro since 1970 01 01 00:00:00 GMT
  1'Line ID
  2'Direction
  3'Journey Pattern ID
  4'Time Frame (The start date of the production time table - in Dublin the production time table starts at 6am and ends at 3am)
  5'Vehicle Journey ID (A given run on the journey pattern)
  6'Operator (Bus operator, not the driver)
  7'Congestion [0=no,1=yes]
  8'Lon WGS84
  9'Lat WGS84
  10'Delay (seconds, negative if bus is ahead of schedule)
  11'Block ID (a section ID of the journey pattern)
  12'Vehicle ID
  13'Stop ID
  14'At Stop [0=no,1=yes]
"""
DATA_HEADER = ['UnixTimestamp', 'LineID', 'Direction', 'JourneyPatternID', 'TimeFrame', \
               'VehicleJourneyID', 'Operator', 'Congestion', 'Lon', 'Lat', 'Delay', \
                'BlockID', 'VehicleID', 'StopID', 'AtStop']

CSV_PATH = 'dublin-dataset/dublin-dataset/siri.20121106.csv'

CONN = pymongo.MongoClient('localhost', 27017)

def safe_int_cast(val, default=None):
    try:
        return int(val)
    except(ValueError, TypeError):
        return None

def insert_into_mongo(data_dict_list, drop_if_exists=True):
    db = CONN.dublin_bus_dataset
    collection = db.bus_gps_data
    if drop_if_exists:
        if collection.count() > 0:
            collection.drop()
        collection = db.bus_gps_data
    collection.insert_many(data_dict_list)    

def data_dict_list_from_csv():
    rows_read = 0
    out_list = []
    with open(CSV_PATH) as f:
        reader = csv.reader(f, skipinitialspace=True)
        for i, row in enumerate(reader):
            row_dict = {}
            row_dict[DATA_HEADER[0]] = safe_int_cast(row[0])
            row_dict[DATA_HEADER[1]] = safe_int_cast(row[1])
            row_dict[DATA_HEADER[2]] = safe_int_cast(row[2])
            row_dict[DATA_HEADER[3]] = str(row[3])
            row_dict[DATA_HEADER[4]] = datetime.strptime(row[4], '%Y-%m-%d')
            row_dict[DATA_HEADER[5]] = safe_int_cast(row[5])
            row_dict[DATA_HEADER[6]] = str(row[6])
            row_dict[DATA_HEADER[7]] = True if safe_int_cast(row[7]) == 1 else False
            row_dict[DATA_HEADER[8]] = float(row[8])
            row_dict[DATA_HEADER[9]] = float(row[9])
            row_dict[DATA_HEADER[10]] = safe_int_cast(row[10])
            row_dict[DATA_HEADER[11]] = safe_int_cast(row[11])
            row_dict[DATA_HEADER[12]] = safe_int_cast(row[12])
            row_dict[DATA_HEADER[13]] = safe_int_cast(row[13])
            row_dict[DATA_HEADER[14]] = True if safe_int_cast(row[14]) == 1 else False
            out_list.append(row_dict)
            rows_read += 1
            if rows_read % 100000 == 0:
                print('Read rows so far:', rows_read)
    return out_list


if __name__ == '__main__':
    rows = data_dict_list_from_csv()
    print('Read all rows')
    insert_into_mongo(rows)
    print('All rows inserted')
    print('Inserted', len(rows), 'from csv into mongodb')
